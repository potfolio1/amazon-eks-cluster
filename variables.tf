
variable "region" {
  description = "AWS region to deploy to"
  type        = string
}

variable "cluster_name" {
  description = "EKS cluster name"
  type = string
}